import React, {Component} from 'react';
import PropTypes from 'prop-types';

/**
 * MatchMedia
 * @description [Description]
 * @example
  <div id="MatchMedia"></div>
  <script>
    ReactDOM.render(React.createElement(Components.MatchMedia, {
    	title : 'Example MatchMedia'
    }), document.getElementById("MatchMedia"));
  </script>
 */
export default class MatchMedia extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isVisible: false
		};

		this.mql = null;
		this.onMatch = mql => this._onMatch(mql);
	}

	_onMatch(mql) {
		const isVisible = !!mql.matches;
		this.setState({isVisible});
	}

	componentDidMount() {
		if (!window.matchMedia) {
			return;
		}

		const {query} = this.props;

		this.mql = window.matchMedia(query);
		this.mql.addListener(this.onMatch);
		this.onMatch(this.mql);
	}

	componentWillUnmount() {
		this.mql && this.mql.removeListener(this.onMatch);
	}

	render() {
		const {children, fallback} = this.props;
		const {isVisible} = this.state;

		return isVisible ? children : fallback;
	}
}

MatchMedia.defaultProps = {
	children: null,
	fallback: null,
	query: null
};

MatchMedia.propTypes = {
	children: PropTypes.node,
	fallback: PropTypes.node,
	query: PropTypes.string.isRequired
};
